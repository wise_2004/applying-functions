CREATE OR REPLACE PROCEDURE new_movie(p_title VARCHAR)
LANGUAGE plpgsql AS $$
DECLARE
    v_language_id INTEGER;
BEGIN
    SELECT language_id INTO v_language_id
    FROM language
    WHERE name = 'Klingon';
    IF v_language_id IS NULL THEN
        RAISE EXCEPTION 'Language does not exist';
    END IF;
    INSERT INTO film (
        title,
        rental_rate,
        rental_duration,
        replacement_cost,
        release_year,
        language_id
    ) VALUES (
        p_title,
        4.99,
        3,
        19.99,
        EXTRACT(YEAR FROM CURRENT_DATE)::INTEGER,
        v_language_id
    );
END; $$;

