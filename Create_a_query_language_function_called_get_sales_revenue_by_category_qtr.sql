CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(p_current_quarter text)
RETURNS TABLE(category text, total_sales_revenue numeric) AS $$
BEGIN
    RETURN QUERY
    SELECT
        c.name,
        SUM(p.amount)
    FROM
        payment p
    JOIN
        rental r ON p.rental_id = r.rental_id
    JOIN
        inventory i ON r.inventory_id = i.inventory_id
    JOIN
        film f ON i.film_id = f.film_id
    JOIN
        film_category fc ON f.film_id = fc.film_id
    JOIN
        category c ON fc.category_id = c.category_id
    WHERE
        date_trunc('quarter', p.payment_date) = date_trunc('quarter', CURRENT_DATE)
        AND c.name IS NOT NULL
    GROUP BY
        c.name;

END;
$$ LANGUAGE plpgsql;
